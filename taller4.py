redis 128.0.0.1:6382> ping
PONG
128.0.0.1:6382> SET mouse dispositivo
OK
128.0.0.1:6382> SET monitor pantalla
OK
128.0.0.1:6382> SET laptop computadora
OK
128.0.0.1:6382> SET usb  dispositivo
OK
128.0.0.1:6382> mget mouse
1) "dispsitivo"
128.0.0.1:6382> mget monitor
1) (nil)
128.0.0.1:6382> mget laptop
1) "laptop"
128.0.0.1:6382> mget usb
1) "dispositivo"
128.0.0.1:6382> mget mouse monitor usb
1) "dispositivo"
2) "pantalla"
3) "computadora"
128.0.0.1:6382> HSET asig1 taller3 "tarea 4"
(integer) 1
128.0.0.1:6382> HSET asig1 tarea4 "haciendo tarea 5"
(integer) 1
128.0.0.1:6382> HSET asig1 "parcial 2021-1"
(integer) 1
128.0.0.1:6382> HGETALL asig1
1) "tarea4"
2) "haciendo tarea 5"
3) "parcial1"
4) "haciendo parcial 2021-1"
5) "tarea"
6) "parcial 2021-1"
128.0.0.1:6382> BGSAVE
Background saving started
128.0.0.1:6382> hum.rdb
